import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import store  from './stores/store';

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {requiresAuth: true},
      component: Home
    },
    {
      path: '/signup',
      name: 'sign-up',
      meta: {closeForAuth: true},
      component: () => import('./components/SignUp')
    },
    {
      path: '/signin',
      name: 'sign-in',
      meta: {closeForAuth: true},
      component: () => import('./components/Navigation')
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.auth.isAuth == false) {
      next({
        path: '/signin',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.closeForAuth)) {
    if (store.state.auth.isAuth != false && store.state.auth.isAuth != undefined) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next();
  }
});

export default router;