import * as types from "./types";
import * as mutation_types from "./mutation_types";

export default {
  [mutation_types.M_SIGN_IN](state, payload) {
    console.log(payload);
    state.isAuth = true;
    state.user = payload;
  },
  [mutation_types.M_SIGN_UP](state, payload) {
    state.users.push(payload);
  },
  [mutation_types.M_CHANGE_FORM](state, payload) {
    state.formChange = payload;
  }
};
