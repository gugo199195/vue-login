import * as types from "./types";
import * as mutation_types from "./mutation_types";
import state from "./state";

export default {
  [types.A_SIGN_IN](context, payload) {
    return new Promise((resolve, reject) => {
      context.state.users.filter(val => {
        if(val.email == payload.email && val.password == payload.password) {
          resolve(context.commit(mutation_types.M_SIGN_IN, val));
        }
      })
    })
  },
  [types.A_SIGN_UP](context, payload) {
    return new Promise((resolve, reject) => {
      resolve(context.commit(mutation_types.M_SIGN_UP, payload));
    })
  },
  [types.A_CHANGE_FORM](context, payload) {
    context.commit(mutation_types.M_CHANGE_FORM, payload)
  },
};
